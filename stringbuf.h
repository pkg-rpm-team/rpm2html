/*
 * stringbuf.h: interface for the String buffering module.
 */

#ifndef __STRINGBUF_H__
#define __STRINGBUF_H__

const char *stringAdd(const char *str);
void  stringFree(const char *str);

#endif /* __STRINGBUF_H__ */
