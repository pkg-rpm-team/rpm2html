/*
 * memory.h: interface for the memory allocation debugger.
 *
 * daniel@veillard.com
 */


#ifndef _RPM2HTML_DEBUG_MEMORY_ALLOC_
#define _RPM2HTML_DEBUG_MEMORY_ALLOC_

/* #define NO_DEBUG_MEMORY */
#include <libxml/xmlmemory.h>
#include <libxml/parser.h>

#endif  /* _RPM2HTML_DEBUG_MEMORY_ALLOC_ */

