;
; Automatically generated rpm2html 0.1 language file
;
.html

Generado por

index.html

Groups.html

ByDate.html

ByName.html

Vendors.html

Distribs.html

Bienvenido a la base de datos de RPMs de 

<p>
<strong>rpm2html</strong> genera autom&aacute;ticamente paginas web describiendo paquetes 
<a href="http://www.rpm.org/">RPM</a>.</p>
<p>
El objetivo de rpm2html es identificar las dependencias entre distintos 
paquetes y facilitar asi el acceso a los recursos necesarios para la 
instalaci&oacute;n de un paquete en particular. Cada paquete es analizado para conocer sus
 dependencias y utilidades que ofrece. Estas relaciones son reflejadas empleando
vinculos (links) a las distintas paginas generadas. De ese modo encontrar el 
paquete que necesita es simplemente cuesti&oacute;n de unos pocos clics!</P> 
<p>
Estas paginas permiten una mayor comodida a la hora de encontrar los
paquetes buscados instantaneamente, conociendo las funciones del paquete
(en la medida que el creador del paquete lo comente correctamente).</P> 

Este archivo tiene registrados %d RPMs (%d MBytes de datos) 

En este ordenador est&aacute;n instalados %d RPMs (%d MBytes de datos)

La lista de 

RPM indexados por categorias

RPM indexados por fecha de creaci&oacute;n

RPM indexados por nombre

RPM indexados por gestor del paquete 

RPM indexados por distribuci&oacute;n

RPM indexados por la fecha de instalaci&oacute;n

Lugares donde encontrar los c&oacute;digos fuentes

Mirror local

Mirrors

Tiempo de generaci&oacute;n

segundos

Bienvenido a la descripci&oacute;n RPM de 

De

Nombre

Distribuci&oacute;n

Versi&oacute;n

Distribuidor

Revisi&oacute;n

Fecha de compilaci&oacute;n

Fecha de instalaci&oacute;n

Grupo

M&aacute;quina de compilaci&oacute;n

Tama&ntilde;o

RPM del c&oacute;digo fuente 

Mantenido por

Url

Descripci&oacute;n

Proporciona

Requerimientos

Copyright

Ficheros

No hay lista de ficheros en el paquete !

No hay Sumario!

Recursos RPM

Distribuido por

RPMs ordenados por Grupo

RPMs del Grupo 

RPMs ordenados por Distribuci&oacute;n 

RPMs de la Distribuci&oacute;n 

RPMs ordenados por distribuidor 

RPMs distribuidos por 

RPMs ordenados por fecha de creaci&oacute;n

RPMs ordenados por fecha de instalaci&oacute;n

RPMs de menos de 3 dias de antig&uuml;edad

RPMs de menos de 1 semana de antig&uuml;edad

RPMs de menos de 2 semanas de antig&uuml;edad

RPMs de menos de 1 mes de antig&uuml;edad

RPMS de mas de 1 mes de antig&uuml;edad

RPMs instalados hace menos de 3 dias

RPMs instalados hace menos de 1 semana

RPMs instalados hace menos de 2 semanas

RPMs instalados hace menos de 1 mes

RPMs instalados hace mas de 1 mes 

RPMs ordenados por Nombre 

No hay Descripcion !

Desconocido

Ninguno

grupo/desconocido

host.desconocido

Indice

Paquetes que comienzan por

Atenci&oacute;n este paquete no devuelve listas de recursos v&aacute;lidas 

Intente escoger otro

M&aacute;s

Log de cambios

Subdirectorios

Tree.html

Navegar por el &aacute;rbol de directorios


