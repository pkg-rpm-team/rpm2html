rpm2html (1.11.2-10) UNRELEASED; urgency=medium

  * Use HTTPS in the Vcs-* fields, and use the cgit frontend instead of
    gitweb.

 -- Michal Čihař <nijel@debian.org>  Tue, 20 Sep 2016 13:39:24 +0200

rpm2html (1.11.2-9) unstable; urgency=medium

  * Bump standards to 3.9.8.
  * Update Maintainer, Uploaders and VCS for pkg-rpm team.

 -- Michal Čihař <nijel@debian.org>  Wed, 27 Jul 2016 12:48:54 +0200

rpm2html (1.11.2-8) unstable; urgency=medium

  * Fix build failure with recent glibc (Closes: #807395, #807396).

 -- Michal Čihař <nijel@debian.org>  Tue, 08 Dec 2015 17:00:23 +0100

rpm2html (1.11.2-7) unstable; urgency=medium

  * Adjust Vcs-Svn URL.
  * Bump standards to 3.9.6.

 -- Michal Čihař <nijel@debian.org>  Mon, 06 Oct 2014 09:28:07 +0200

rpm2html (1.11.2-6) unstable; urgency=low

  * Bump standards to 3.9.5.
  * Fix parsing of new CVE syntax (Closes: #731440).
  * Include sample configurations within the package.

 -- Michal Čihař <nijel@debian.org>  Tue, 25 Feb 2014 14:30:32 +0100

rpm2html (1.11.2-5) unstable; urgency=low

  * Fixed crash with empty config file.
  * Fixed typos in manpage.
  * Use anonscm URL in debian/control.

 -- Michal Čihař <nijel@debian.org>  Mon, 01 Jul 2013 14:46:07 +0200

rpm2html (1.11.2-4) unstable; urgency=low

  * Bump standards to 3.9.4.
  * Use debhelper 9.
  * Use dh_autoreconf.

 -- Michal Čihař <nijel@debian.org>  Thu, 06 Jun 2013 13:52:05 +0200

rpm2html (1.11.2-3) unstable; urgency=low

  * Bump standards to 3.9.3.
  * Adjust debian/copyright to match final copyright format
    specification.
  * Add build-{arch,indep} target to debian/rules.

 -- Michal Čihař <nijel@debian.org>  Mon, 04 Jun 2012 09:33:36 +0200

rpm2html (1.11.2-2) unstable; urgency=low

  * Fix build with rpm 4.9 (Closes: #627396).
  * Bump standards to 3.9.2.

 -- Michal Čihař <nijel@debian.org>  Fri, 20 May 2011 16:30:06 +0200

rpm2html (1.11.2-1) unstable; urgency=low

  * New upstream version.

 -- Michal Čihař <nijel@debian.org>  Mon, 15 Nov 2010 09:04:03 +0100

rpm2html (1.11.1-1) unstable; urgency=low

  * New upstream release.
    - All patches intergrated upstream.
  * Bump standards to 3.9.1.

 -- Michal Čihař <nijel@debian.org>  Tue, 05 Oct 2010 11:03:21 +0200

rpm2html (1.11.0-4) unstable; urgency=low

  * Fix typos in man page.
  * Convert to 3.0 (quilt) source format.
  * Bump standards to 3.8.3

 -- Michal Čihař <nijel@debian.org>  Sun, 03 Jan 2010 14:05:20 +0100

rpm2html (1.11.0-3) unstable; urgency=low

  * Bump standards to 3.8.2 (no changes).

 -- Michal Čihař <nijel@debian.org>  Thu, 09 Jul 2009 16:40:09 +0200

rpm2html (1.11.0-2) unstable; urgency=low

  * Rebuild against rpm 4.7.0.

 -- Michal Čihař <nijel@debian.org>  Mon, 15 Jun 2009 12:17:45 +0200

rpm2html (1.11.0-1) unstable; urgency=low

  * New upstream version.
  * Our single patch was merged upstream.
  * Bump standards to 3.8.1 (no changes).

 -- Michal Čihař <nijel@debian.org>  Mon, 11 May 2009 14:43:34 +0200

rpm2html (1.9.7-2) unstable; urgency=low

  * Include some more documentation in binary package (including NEWS).
  * Fix author email in debian/copyright.
  * Add missing ${misc:Depends} dependency.
  * Update debian/copyright (year, link tp GPL-2).

 -- Michal Čihař <nijel@debian.org>  Sun, 15 Feb 2009 14:07:43 +0100

rpm2html (1.9.7-1) unstable; urgency=low

  * New upstream version.
  * Switch Vcs-Browser to viewsvn.

 -- Michal Čihař <nijel@debian.org>  Thu, 04 Dec 2008 11:16:33 +0100

rpm2html (1.9.6-2) unstable; urgency=low

  * Add Vcs-* headers.
  * Add Homepage: header.
  * Update debian/copyright to match current license and convert it to machine
    readable format.

 -- Michal Čihař <nijel@debian.org>  Thu, 23 Oct 2008 16:53:25 +0200

rpm2html (1.9.6-1) unstable; urgency=low

  * New maintainer (Closes: #465897).
  * New upstream version.
  * Add watch file.
  * Do not include generated files in diff.
  * Use quilt for patches.
    - Convert man page fixes to separate patch.
  * Switch to debhelper 7.
  * Update to standards 3.8.0.
  * Added debian/README.source.
  * Cleanup generated files. 
  * Force generating of configure at start of build (it will be regerated
    anyway).

 -- Michal Čihař <nijel@debian.org>  Thu, 23 Oct 2008 16:33:37 +0200

rpm2html (1.9.2-2) unstable; urgency=low

  * QA upload.
    + Set maintainer to QA Group
  * Add depends on rpm, since rpm2html fails to start without it,
    and shlibs:Depends only adds a dependency on librpm. (Closes: #487657)
  * Fix funny character in last changelog entry. (Hopefully Closes: #349826)
  * Lintian noted:
    + move dh compat from debian/rules to debian/compat
    + manpage syntax fixes
    + remove useless debian/dirs which creates an empty directory
    + don't ignore errors from distclean

 -- Frank Lichtenheld <djpig@debian.org>  Tue, 01 Jul 2008 02:27:44 +0200

rpm2html (1.9.2-1) unstable; urgency=low

  * New upstream release
  * Solved undefined reference to `rpmBuildFileList' (Closes: #339872).
  * Updated config.guess and config.sub (Closes: #342429).

 -- Angel Ramos <seamus@debian.org>  Mon,  9 Jan 2006 18:41:12 +0100

rpm2html (1.8.1-1) unstable; urgency=low

  * New upstream release
  * Fixed parse.h error (Closes: #192932).

 -- Angel Ramos <seamus@debian.org>  Wed, 6 Aug 2003 18:10:00 +0100

rpm2html (1.7-3) unstable; urgency=low

  * Fixed configure.in and added libbz2-dev as build-dep. Now the package
    builds with success (Closes: #165303).

 -- Angel Ramos <seamus@debian.org>  Wed, 23 Oct 2002 18:00:00 +0200

rpm2html (1.7-2) unstable; urgency=low

  * Rebuilt with librpm4 (Closes: #128196).
  * Fixed configure.in in order to support librpm4.

 -- Angel Ramos <seamus@debian.org>  Tue, 8 Jan 2002 11:04:00 +0100

rpm2html (1.7-1.1) unstable; urgency=low

  * Fixed spelling error in description (Closes: #125323).

 -- Angel Ramos <seamus@debian.org>  Wed, 19 Dec 2001 03:27:00 +0100

rpm2html (1.7-1) unstable; urgency=low

  * New upstream release

 -- Angel Ramos <seamus@debian.org>  Thu, 11 Oct 2001 11:12:49 +0100

rpm2html (1.5-3) unstable; urgency=low

  * New maintainer (Closes: #90880).
 
 -- Angel Ramos <seamus@salix.org>  Mon,  16 Jul 2001 16:23:25 +0100

rpm2html (1.5-2) unstable; urgency=low

  * Lowered priority to extra, correcting the override disparity.

 -- Matej Vela <vela@debian.org>  Tue, 17 Apr 2001 13:03:17 +0200

rpm2html (1.5-1) unstable; urgency=low

  * New upstream version.
  * Package is orphaned (see #90880); maintainer set to Debian QA Group.
  * Converted to debhelper.  Closes: #91019.
  * Conforms to Standards version 3.5.2:
    * Added build dependencies.
    * debian/rules: Support the `debug' build option.
  * debian/copyright: Updated.
  * debian/README.debian: Contained only duplicate information; removed.

 -- Matej Vela <vela@debian.org>  Sun,  1 Apr 2001 16:29:19 +0200

rpm2html (0.70p1-1.1) frozen unstable; urgency=low

  * NMU, rebuilt to fix rc bug (closes: 64414)
  * Various small changes to work with new librpm <sigh>

 -- Randolph Chung <tausq@debian.org>  Sun, 25 Jun 2000 10:17:17 -0700

rpm2html (0.70p1-1) unstable; urgency=low

  * Initial Release.

 -- Jim Pick <jim@jimpick.com>  Thu, 12 Mar 1998 21:05:32 -0800

Local variables:
mode: debian-changelog
End:
